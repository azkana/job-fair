
<li class="treeview @if($adminActiveMenu == 'jobfair') active @endif">
    <a href="#">
        <i class="fa fa-globe"></i>
        <span>Job Fair</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
            <span class="label label-danger pull-right">
                New
            </span>
        </span>
    </a>
    <ul class="treeview-menu">
        <li class="@if($adminActiveSubMenu == 'master') active @endif">
            <a href="#">
                <i class="fa fa-circle-o"></i> Master
                <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>
            <ul class="treeview-menu">
                <li class="@if($urlSegment4 == 'participant-type') active @endif">
                    <a href="{!! route('jobfair.participant-type.index') !!}">
                        <i class="fa fa-circle-o"></i> Participant Type
                        <span class="pull-right-container"></span>
                    </a>
                </li>
            </ul>
        </li>
        {{-- <li class="@if($adminActiveSubMenu == 'master') active @endif">
            <a href="{!! route('shortener.url.index') !!}">
                <i class="fa fa-circle-o"></i> Master
                <span class="pull-right-container"></span>
            </a>
        </li> --}}
    </ul>
</li>