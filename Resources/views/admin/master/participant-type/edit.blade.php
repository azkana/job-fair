@extends('jobfair::admin.layouts.master')

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title"></h3>
                <div class="box-tools pull-right">
                    <a class="btn btn-sm btn-danger" href="{!! route('jobfair.participant-type.index') !!}">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            {!! Form::open([ 'route' => ['jobfair.participant-type.update', $data->id], 'class' => 'form-horizontal', 'autocomplete' => 'off' ]) !!}
                <div class="box-body">
                    <div class="form-group">
                        <label for="code" class="col-sm-2 control-label">Code</label>
                        <div class="col-sm-3">
                            {!! Form::text('code', $data->code, [ 'class' => 'form-control', 'required', 'readonly']) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="desc" class="col-sm-2 control-label">Description</label>
                        <div class="col-sm-5">
                            {!! Form::textarea('desc', $data->desc, [ 'class' => 'form-control', 'required', 'rows' => 2, 'maxlength' => 200]) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="amount" class="col-sm-2 control-label">Amount</label>
                        <div class="col-sm-3">
                            {!! Form::text('amount', $data->amount, [ 'class' => 'form-control text-right number', 'required']) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="quantity" class="col-sm-2 control-label">Quantity</label>
                        <div class="col-sm-3">
                            {!! Form::number('quantity', $data->quantity, [ 'class' => 'form-control text-right', 'required']) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="start_date" class="col-sm-2 control-label">Start Date</label>
                        <div class="col-sm-3">
                            {!! Form::text('start_date', $data->start_date, [ 'class' => 'form-control datepicker']) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="end_date" class="col-sm-2 control-label">End Date</label>
                        <div class="col-sm-3">
                            {!! Form::text('end_date', $data->end_date, [ 'class' => 'form-control datepicker']) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="is_active" class="col-sm-2 control-label">Active? </label>
                        <div class="col-sm-5">
                            <div class="checkbox">
                                <label>
                                    {!! Form::checkbox('is_active', true, $data->is_active) !!} Yes
                                </label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="box-footer">
                    <button type="reset" class="btn btn-default">Cancel</button>
                    <button type="submit" class="btn btn-info pull-right">Update</button>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@endsection

@section('css')
    <style>
        .datepicker table tr td.disabled{
            color:red;
            cursor: not-allowed;
        }
        .datepicker table tr td.today{
            background: orange;
        }
        .datepicker table tr td.active{
            font-weight: bold;
        }
    </style>
@endsection

@section('scripts')
<script>
    $(document).ready(function() {
        $(".number").number(true, 0);
        $('.datepicker').datepicker({
            format: 'dd-mm-yyyy',
            autoclose: true,
            todayHighlight: true,
            weekStart: 1,
            startDate: new Date(),
            language: 'id'
        });
    });
</script>
@endsection