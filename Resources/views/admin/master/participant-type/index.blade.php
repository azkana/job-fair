@extends('jobfair::admin.layouts.master')

@section('content')
    
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title"></h3>
                <div class="box-tools pull-right">
                    {{-- @can('participant-type-create') --}}
                        <a class="btn btn-sm btn-success" href="{!! route('jobfair.participant-type.create') !!}">
                            <i class="fa fa-plus"></i> New Participant Type
                        </a>
                    {{-- @endcan --}}
                </div>
            </div>
            <div class="box-body" style="min-height: 520px">
                <table id="grid-participant-type" class="table table-bordered table-striped table-responsive table-condensed">
                    <thead>
                        <tr>
                            <th style="width: 5%">No.</th>
                            <th>Code</th>
                            <th>Description</th>
                            <th>Amount</th>
                            <th>Quantity</th>
                            <th>Start Date</th>
                            <th>End Date</th>
                            <th>Is Active</th>
                            <th style="width: 10%">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($data as $row)
                            <tr>
                                <td class="text-center">{!! $no++ !!}</td>
                                <td>{!! $row->code !!}</td>
                                <td>{!! $row->desc !!}</td>
                                <td class="text-right">{!! numberFormat($row->amount) !!}</td>
                                <td class="text-right">{!! $row->quantity !!}</td>
                                <td class="text-center">{!! !empty($row->start_date) ? dateFormatDmy($row->start_date) : null !!}</td>
                                <td class="text-center">{!! !empty($row->end_date) ? dateFormatDmy($row->end_date) : null !!}</td>
                                <td class="">{!! $row->is_active == true ? 'Yes' : 'No' !!}</td>
                                <td class="">
                                    @can('participant-type-edit')
                                        <a href="{!! route('jobfair.participant-type.edit', $row->id) !!}" class="btn btn-xs btn-success">Edit</a>
                                    @endcan
                                    @can('participant-type-delete')
                                        {!! Form::open(['method' => 'DELETE','route' => ['jobfair.participant-type.destroy', $row->id],'style'=>'display:inline']) !!}
                                            {!! Form::submit('Delete', ['class' => 'btn btn-xs btn-danger']) !!}
                                        {!! Form::close() !!}
                                    @endcan
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td class="text-danger text-center" colspan="6">No data found.</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@stop
