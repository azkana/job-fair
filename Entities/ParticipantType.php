<?php

namespace Modules\JobFair\Entities;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuid as TraitsUuid;

class ParticipantType extends Model
{
    use TraitsUuid;

    protected $table = 'jf_participant_type';

    public $incrementing    = false;

    protected $casts = [
        'start_date' => 'datetime',
        'end_date' => 'datetime'
    ];

    protected $fillable = [
        'code',
        'desc',
        'amount',
        'quantity',
        'start_date',
        'end_date',
        'is_active'
    ];
    
}
