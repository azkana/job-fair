<?php

namespace Modules\JobFair\Database\Seeders;

use Illuminate\Database\Seeder;
use Modules\JobFair\Entities\ParticipantType;
use Spatie\Permission\Models\Permission;

class ParticipantTypeSeederTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'code' => 'PBC',
                'desc' => 'Public',
                'amount' => 50000,
                'quantity' => 1
            ]
        ];

        foreach($data as $row) {
            $cekData = ParticipantType::where('code', $row['code'])->count();
            if($cekData > 0) {
                $this->command->info('Participant Type code ' . $row['code'] . ' already exists.');
            } else {
                ParticipantType::create([
                    'code' => $row['code'],
                    'desc' => $row['desc'],
                    'amount' => $row['amount'],
                    'quantity' => $row['quantity']
                ]);
                $this->command->info('Participant Type code ' . $row['code'] . ' created successfully');
            }
        }

        /**
         * create permission
         */
        $permissions = [
            ['name' => 'participant-type-list'],
            ['name' => 'participant-type-create'],
            ['name' => 'participant-type-edit'],
            ['name' => 'participant-type-delete']
        ];

        foreach($permissions as $row) {
            $cekData = Permission::where('name', $row['name'])->where('guard_name', 'web')->count();
            if($cekData > 0) {
                $this->command->info('Permission name ' . $row['name'] . ' already exists.');
            } else {
                Permission::create([
                    'name' => $row['name']
                ]);
                $this->command->info('Permission name ' . $row['name'] . ' created successfully');
            }
        }
    }
}
