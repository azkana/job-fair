<?php

namespace Modules\JobFair\Http\Controllers\Admin\Master;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\JobFair\Entities\ParticipantType;

class ParticipantTypeController extends Controller
{
    protected $pageTitle;

    public function __construct()
    {
        $this->pageTitle = 'Participant Type';
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $params['pageTitle']    = $this->pageTitle;
        $params['no']           = 1;
        $params['data']         = ParticipantType::all();
        return view('jobfair::admin.master.participant-type.index', $params);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $params['pageTitle']    = $this->pageTitle;
        return view('jobfair::admin.master.participant-type.create', $params);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        try {
            $data['code']   = trim($request->code);
            $data['desc']   = trim($request->desc);
            $data['amount'] = cleanNominal($request->amount);
            $data['quantity']   = cleanNominal($request->quantity);
            $data['start_date'] = !empty($request->start_date) ? dateFormatYmd($request->start_date) : null;
            $data['end_date']   = !empty($request->end_date) ? dateFormatYmd($request->end_date) : null;
            $data['is_active']  = $request->is_active == null ? 0 : 1;

            ParticipantType::create($data);
            return redirect(route('jobfair.participant-type.index'))
				->with('success', trans('message.success_save'));
        } catch (\Exception $e) {
            return redirect(route('jobfair.participant-type.index'))
				->with('error', trans('message.error_save'));
        }
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $params['pageTitle']    = $this->pageTitle;
        $params['data']         = ParticipantType::findOrFail($id);
        return view('jobfair::admin.master.participant-type.edit', $params);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        try {
            $data['desc']   = trim($request->desc);
            $data['amount'] = cleanNominal($request->amount);
            $data['quantity']   = cleanNominal($request->quantity);
            $data['start_date'] = !empty($request->start_date) ? dateFormatYmd($request->start_date) : null;
            $data['end_date']   = !empty($request->end_date) ? dateFormatYmd($request->end_date) : null;
            $data['is_active']  = $request->is_active == null ? 0 : 1;
            ParticipantType::findOrFail($id)
                ->update($data);
            return redirect(route('jobfair.participant-type.index'))
				->with('success', trans('message.success_edit'));
        } catch (\Exception $e) {
            return redirect(route('jobfair.participant-type.index'))
				->with('error', trans('message.error_edit'));
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        try {
            ParticipantType::findOrFail($id)
                ->delete();
            return redirect(route('jobfair.participant-type.index'))
				->with('success', trans('message.success_delete'));
        } catch (\Exception $e) {
            return redirect(route('jobfair.participant-type.index'))
				->with('error', trans('message.error_delete'));
        }
    }
}
