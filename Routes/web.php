<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::middleware(['auth'])->group(function () {
    Route::prefix('manage')->group(function() {
        Route::prefix('jobfair')->group(function() {
            Route::prefix('master')->group(function() {

                Route::name('jobfair.')->group(function() {
                    Route::name('participant-type.')->group(function() {
                        Route::get('/participant-type', 'Admin\Master\ParticipantTypeController@index')->name('index');
                        Route::get('/participant-type/create', 'Admin\Master\ParticipantTypeController@create')->name('create');
                        Route::post('/participant-type/create', 'Admin\Master\ParticipantTypeController@store')->name('store');
                        Route::get('/participant-type/{id}/edit', 'Admin\Master\ParticipantTypeController@edit')->name('edit');
                        Route::post('/participant-type/{id}', 'Admin\Master\ParticipantTypeController@update')->name('update');
                        Route::delete('/participant-type/{id}', 'Admin\Master\ParticipantTypeController@destroy')->name('destroy');
                    });
                });
            });
            Route::get('/', 'Admin\JobFairController@index');
        });
    });
});
